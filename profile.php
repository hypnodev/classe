<?php

namespace Classe;
include_once("Start.php");
use DateTime;

session_start();

if(!isset($_SESSION["surname"])) {
	header("location: index.php");
	exit;
}

?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>3°A Informatica</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/default.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<div class="container">

	    <?php include("Templates/header.php"); ?>

	    <div class="panel panel-default">
		  <div class="panel-heading" style="text-align: center;">Profilo di <strong><?php echo($CDatabaseOperations->getUserSurname($CDatabase, $_GET["id"])); ?></strong></div>
		  <div class="panel-body">
				<div class="row">
					<div class="col-xs-4 col-md-2">
		    			<?php echo("<img src=\"".$CDatabaseOperations->getUserAvatar($CDatabase, $CDatabaseOperations->getUserSurname($CDatabase, $_GET["id"]))."\" class=\"avatar\" style=\"width: 128px; height: 128px; \" />"); ?>
		  			</div>
		  			<div class="col-xs-8 col-md-6">
		  				<?php echo($CDatabaseOperations->getUserProperty($CDatabase, $CDatabaseOperations->getUserSurname($CDatabase, $_GET["id"]), "Firstname")." ".$CDatabaseOperations->getUserSurname($CDatabase, $_GET["id"])."<br />
		  				<strong>Data di nascita: </strong>".((new DateTime($CDatabaseOperations->getUserProperty($CDatabase, $CDatabaseOperations->getUserSurname($CDatabase, $_GET["id"]), "Birthdate")))->format('d/m/Y'))."<br />
		  				<strong>Email: </strong> <a href=\"mailto:".$CDatabaseOperations->getUserProperty($CDatabase, $CDatabaseOperations->getUserSurname($CDatabase, $_GET["id"]), "Email")."\">".$CDatabaseOperations->getUserProperty($CDatabase, $CDatabaseOperations->getUserSurname($CDatabase, $_GET["id"]), "Email")."</a>"); ?> 
		  			</div>
		  			<div class="col-xs-6 col-md-4">
		  				<?php 
		  					if($CDatabaseOperations->getUserProperty($CDatabase, $CDatabaseOperations->getUserSurname($CDatabase, $_GET["id"]), "Facebook") !== "") 
		  						echo("<img src=\"images/socials/facebook.png\" /> <a href=\"http://www.youtube.com/channel/".$CDatabaseOperations->getUserProperty($CDatabase, $CDatabaseOperations->getUserSurname($CDatabase, $_GET["id"]), "Facebook")."\">Pagina Facebook</a> <br />"); 
		  					if($CDatabaseOperations->getUserProperty($CDatabase, $CDatabaseOperations->getUserSurname($CDatabase, $_GET["id"]), "Twitter") !== "") 
		  						echo("<img src=\"images/socials/twitter.png\" /> <a href=\"http://www.youtube.com/channel/".$CDatabaseOperations->getUserProperty($CDatabase, $CDatabaseOperations->getUserSurname($CDatabase, $_GET["id"]), "Twitter")."\">Pagina Twitter</a> <br />"); 
		  					if($CDatabaseOperations->getUserProperty($CDatabase, $CDatabaseOperations->getUserSurname($CDatabase, $_GET["id"]), "Youtube") !== "") 
		  						echo("<img src=\"images/socials/youtube.png\" /> <a href=\"http://www.youtube.com/channel/".$CDatabaseOperations->getUserProperty($CDatabase, $CDatabaseOperations->getUserSurname($CDatabase, $_GET["id"]), "Youtube")."\">Canale YouTube</a>");
		  				?>
		  			</div>
		  	  	</div>
		  </div>
		</div>

		<div class="row">
			<div class="<?php echo((($CDatabaseOperations->getUserProperty($CDatabase, $CDatabaseOperations->getUserSurname($CDatabase, $_GET["id"]), "Github") !== "") ? ("col-xs-10 col-md-8") : ("col-xs-16 col-md-14"))); ?>">
				<?php foreach($CDatabase->connectionHandle->query("SELECT * FROM activity WHERE AuthorId = ".$_GET["id"]." ORDER BY Id DESC LIMIT 10") as $result) {
			    	echo("<div class=\"panel panel-default\">
							  <div class=\"panel-heading\">
							    <h3 class=\"panel-title\"><a href=\"profile.php?id=".$CActivity->getActivityInfo($CDatabase, $result["Id"], "AuthorId")."\"><img src=\"".$CDatabaseOperations->getUserAvatar($CDatabase, $_SESSION["surname"])."\" class=\"avatar\" /> ".$CDatabaseOperations->getUserSurname($CDatabase, $CActivity->getActivityInfo($CDatabase, $result["Id"], "AuthorId"))."</a></h3>
							  </div>
							  <div class=\"panel-body\">
						");
							    switch($CActivity->getActivityInfo($CDatabase, $result["AuthorId"], "ActivityId")) {
							    	case Core\Extensions\ActivityId::UPLOAD_FILE:  
							    		echo($CDatabaseOperations->getUserSurname($CDatabase, $CActivity->getActivityInfo($CDatabase, $result["Id"], "AuthorId"))." ha caricato il file <a href=\"download.php?file=Uploads/".$CActivity->getActivityInfo($CDatabase, $result["Id"], "Description")."\">".$CActivity->getActivityInfo($CDatabase, $result["Id"], "Description")."</a>");
							    		break;
							    }
					echo("</div>
							</div>
			    		");
			    } ?>
			</div>
			<?php if($CDatabaseOperations->getUserProperty($CDatabase, $CDatabaseOperations->getUserSurname($CDatabase, $_GET["id"]), "Github") !== "") { ?>
		  	<div class="col-xs-6 col-md-4">
				<div class="panel panel-default">
				  <div class="panel-heading" style="text-align: center;"><img src="images/socials/github.png" /> Ultime repository di GitHub</div>
				  <div class="panel-body">
				    <?php 
					$repo = $CGitHub->repos->listUserRepositories($CDatabaseOperations->getUserProperty($CDatabase, $CDatabaseOperations->getUserSurname($CDatabase, $_GET["id"]), "Github"));

					foreach ($repo as $repos)
						echo("<span class=\"glyphicon glyphicon-book\"></span> <a href=\"".$repos->getHtmlUrl()."\">".$repos->getName()."</a> <br />");
				    ?>
				  </div>
				</div>
			</div>
		  	<?php } ?>
		</div>

	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>