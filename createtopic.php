<?php

namespace Classe;
include_once("Start.php");
use PDO;

session_start();

if(!isset($_SESSION["surname"])) {
    header("location: index.php");
    exit;
}

if(isset($_POST["btnCreate"])) {
    $CTopic->addTopic($CDatabase, $_POST["textTitle"], $CDatabaseOperations->getUserId($CDatabase, $_SESSION["surname"]), str_replace(array("\r", "\n"), '<br />', $_POST["textContent"]));
    header("location: forum.php");
    exit;
}

?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>3°A Informatica</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/default.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<div class="container">

		<?php include("Templates/header.php"); ?>

        <h3>Crea discussione</h3>

        <div class="panel panel-default">
          <div class="panel-heading">Crea discussione</div>
          <div class="panel-body">
            <form method="POST" action="#" accept-charset="UTF-8" autocomplete="off" class="form-horizontal">
                <div class="form-group">
                  <label for="inputEmail" class="col-lg-2 control-label">Titolo</label>
                  <div class="col-lg-10">
                    <input class="form-control" name="textTitle" placeholder="Titolo" type="text">
                  </div>
                </div>
                  <div class="form-group">
                    <label for="textArea" class="col-lg-2 control-label">Contenuto disucssione</label>
                    <div class="col-lg-10">
                      <textarea name="textContent" class="form-control" rows="3" id="textArea" required></textarea>
                      <span class="help-block">I tag html sono abilitati.</span>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                      <button name="btnCreate" type="submit" class="btn btn-primary">Pubblica</button>
                    </div>
                  </div>
              </form>
        </div>

    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>