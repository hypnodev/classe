<?php

namespace Classe;
include_once("Start.php");
use PDO;
use DateTime;

session_start();

$errorMsg = false;

if(isset($_SESSION["surname"])) {
    header("location: uploads.php");
    exit;
}

if(isset($_POST["btnLogin"])) {
    $result = $CDatabase->connectionHandle->prepare("SELECT * FROM users WHERE Surname = ? AND Password = ? LIMIT 1");
    $result->execute(array(trim($_POST["textSurname"]), hash('sha256', "$3ainf$15$!".trim($_POST["textPassword"]))));
    if($result->fetch(PDO::FETCH_ASSOC) > 0) {
        $_SESSION["surname"] = trim($_POST["textSurname"]);
        $logQuery = $CDatabase->connectionHandle->prepare("INSERT INTO accesslogs(Date, Ip, AuthorId) VALUES (?, ?, ?)");
        $logQuery->execute(array((new DateTime())->format("Y-m-d H:i:s"), $CUtils->getClientIP()." (".gethostbyaddr($_SERVER['REMOTE_ADDR']).")", $CDatabaseOperations->getUserId($CDatabase, $_SESSION["surname"])));
        header("location: home.php");
        exit;
    } else $errorMsg = true;
}

?>

<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>3°A Informatica</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container">
        <div class="page-header">
            <h1>3° A Informatica</h1>
        </div>
        <?php if($errorMsg) { ?>
            <div class="alert alert-dismissible alert-danger">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>E' stato riscontrato un errore</strong> Hai inserito dei dati errati!<br />Se non hai ancora un account contatta Cosenza
            </div>
        <?php } ?>
        <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" accept-charset="UTF-8" autocomplete="off" class="form-horizontal">
            <legend>Login</legend>
            <div class="form-group">
              <label for="inputEmail" class="col-lg-2 control-label">Cognome</label>
              <div class="col-lg-10">
                <input class="form-control" name="textSurname" placeholder="Cognome" type="text" required />
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword" class="col-lg-2 control-label">Password</label>
              <div class="col-lg-10">
                <input class="form-control" name="textPassword" placeholder="Password" type="password" required />
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-10 col-lg-offset-2">
                <input type="reset" name="btnReset" class="btn btn-default" value="Reset" />
                <input type="submit" name="btnLogin" class="btn btn-primary" value="Login" />
              </div>
            </div>
        </form>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>