<?php

namespace Classe;
include_once("Start.php");
use PDO;
use DateTime;

session_start();

$errorMsg = "";

if(!isset($_SESSION["surname"])) {
	header("location: index.php");
	exit;
}

if(isset($_POST["btnChangePassword"])) {
	$result = $CDatabase->connectionHandle->prepare("SELECT * FROM users WHERE Surname = ? AND Password = ? LIMIT 1");
    $result->execute(array($_SESSION["surname"], hash('sha256', "$3ainf$15$!".trim($_POST["textPassword"]))));
    if($result->fetch(PDO::FETCH_ASSOC) > 0) {
    	if(trim($_POST["textNewPassword"]) === trim($_POST["textConfirmPassword"])) {
    		$CDatabase->connectionHandle->exec("UPDATE users SET Password = '".hash('sha256', "$3ainf$15$!".trim($_POST["textNewPassword"]))."' WHERE Surname = '".$_SESSION["surname"]."'");
    	} else $errorMsg = "Le password non coincidono!";
    } else $errorMsg = "L'attuale password è errata!";
}
else if(isset($_POST["btnChangeSettings"])){
    $CDatabase->connectionHandle->exec("UPDATE users SET Email='".$_POST["textEmail"]."', Facebook='".$_POST["textFacebook"]."', Twitter='".$_POST["textTwitter"]."', Youtube='".$_POST["textYoutube"]."', GitHub='".$_POST["textGitHub"]."', Birthdate='".((new DateTime($_POST["textBirthdate"]))->format('Y-m-d'))."', Avatar='".$_POST["textAvatar"]."' WHERE Surname = '".$_SESSION["surname"]."'");
}

?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>3°A Informatica</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/default.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<div class="container">

		<?php include("Templates/header.php"); ?>

		<?php if($errorMsg !== "") { ?>
            <div class="alert alert-dismissible alert-danger">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>E' stato riscontrato un errore</strong><br /><?php echo($errorMsg); ?>
            </div>
        <?php } ?>

        <?php if(($errorMsg === "" && isset($_POST["btnChangePassword"])) || ($errorMsg === "" && isset($_POST["btnChangeSettings"]))) { ?>
            <div class="alert alert-dismissible alert-success">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>Operazione completata!</strong><br />Operazione completata con successo!
            </div>
        <?php } ?>

		<div class="panel panel-default">
		  <div class="panel-heading" style="text-align: center;">Impostazioni di <strong><?php echo($_SESSION["surname"]); ?></strong></div>
		  <div class="panel-body">
				<div class="row">
					<div class="col-xs-4 col-md-2">
		    			<?php echo("<img src=\"".$CDatabaseOperations->getUserAvatar($CDatabase, $_SESSION["surname"])."\" class=\"avatar\" style=\"width: 128px; height: 128px; \" />"); ?>
		  			</div>
		  			<div class="col-xs-8 col-md-6">
		  				<?php echo($CDatabaseOperations->getUserProperty($CDatabase, $_SESSION["surname"], "Firstname")." ".$_SESSION["surname"]."<br />
		  				<strong>Data di nascita: </strong>".((new DateTime($CDatabaseOperations->getUserProperty($CDatabase, $_SESSION["surname"], "Birthdate")))->format('d/m/Y'))."<br />
		  				<strong>Email: </strong> <a href=\"mailto:".$CDatabaseOperations->getUserProperty($CDatabase, $_SESSION["surname"], "Email")."\">".$CDatabaseOperations->getUserProperty($CDatabase, $_SESSION["surname"], "Email")."</a> <br />
                        <a style=\"cursor: pointer;\" data-toggle=\"modal\" data-target=\"#boxLogs\">Vedi ultimi accessi</a>"); ?>
		  			</div>
		  			<div class="col-xs-6 col-md-4">
		  				<?php 
		  					if($CDatabaseOperations->getUserProperty($CDatabase, $_SESSION["surname"], "Facebook") !== "") 
		  						echo("<img src=\"images/socials/facebook.png\" /> <a href=\"http://www.facebook.com/".$CDatabaseOperations->getUserProperty($CDatabase, $_SESSION["surname"], "Facebook")."\">Pagina Facebook</a> <br />"); 
		  					if($CDatabaseOperations->getUserProperty($CDatabase, $_SESSION["surname"], "Twitter") !== "") 
		  						echo("<img src=\"images/socials/twitter.png\" /> <a href=\"http://www.twitter.com/".$CDatabaseOperations->getUserProperty($CDatabase, $_SESSION["surname"], "Twitter")."\">Pagina Twitter</a> <br />"); 
		  					if($CDatabaseOperations->getUserProperty($CDatabase, $_SESSION["surname"], "Youtube") !== "") 
		  						echo("<img src=\"images/socials/youtube.png\" /> <a href=\"http://www.youtube.com/user/".$CDatabaseOperations->getUserProperty($CDatabase, $_SESSION["surname"], "Youtube")."\">Canale YouTube</a>");
		  				?>
		  			</div>
		  	  	</div>
		  </div>
		</div>

		<div class="panel panel-default">
		  <div class="panel-heading" style="text-align: center;">Cambia password</strong></div>
		  <div class="panel-body">
				<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" accept-charset="UTF-8" autocomplete="off" class="form-horizontal">
				    <legend><h5>(Per non cambiare la password lascia tutti i campi vuoti)</h5></legend>
				    <div class="form-group">
				      <label for="inputEmail" class="col-lg-2 control-label">Password attuale:</label>
				      <div class="col-lg-10">
				        <input class="form-control" name="textPassword" placeholder="Password attuale" type="password" required>
				      </div>
				    </div>
				    <div class="form-group">
				      <label for="inputEmail" class="col-lg-2 control-label">Nuova password:</label>
				      <div class="col-lg-10">
				        <input class="form-control" name="textNewPassword" placeholder="Nuova password" type="password" required>
				      </div>
				    </div>
				    <div class="form-group">
				      <label for="inputEmail" class="col-lg-2 control-label">Conferma password:</label>
				      <div class="col-lg-10">
				        <input class="form-control" name="textConfirmPassword" placeholder="Conferma password" type="password" required>
				      </div>
				    </div>
				    <div class="form-group">
				      <div class="col-lg-10 col-lg-offset-2">
				        <button type="reset" class="btn btn-default">Reset</button>
				        <button name="btnChangePassword" type="submit" class="btn btn-primary">Cambia</button>
				      </div>
				    </div>
				</form>
		  </div>
		</div>

		<div class="panel panel-default">
		  <div class="panel-heading" style="text-align: center;">Account</strong></div>
		  <div class="panel-body">
				<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" accept-charset="UTF-8" autocomplete="off" class="form-horizontal">
				    <div class="form-group">
				      <label for="inputEmail" class="col-lg-2 control-label">Email:</label>
				      <div class="col-lg-10">
				        <input class="form-control" name="textEmail" placeholder="example@domain.ext" type="text">
				      </div>
				    </div>
				    <div class="form-group">
				      <label for="inputEmail" class="col-lg-2 control-label">Data di nascita:</label>
				      <div class="col-lg-10">
				        <input class="form-control" name="textBirthdate" placeholder="01-01-1970" type="text">
				      </div>
				    </div>
				    <div class="form-group">
				      <label for="inputEmail" class="col-lg-2 control-label">Profilo facebook:</label>
				      <div class="col-lg-10">
				        <input class="form-control" name="textFacebook" placeholder="cristian.cosenza" type="text">
				        <span class="help-block">Inserisci solamente il nome utente che appare nella barra degli indirizzi, escludendo http://facebook.com/ (es. cristian.cosenza)</span>
				      </div>
				    </div>
				    <div class="form-group">
				      <label for="inputEmail" class="col-lg-2 control-label">Profilo twitter:</label>
				      <div class="col-lg-10">
				        <input class="form-control" name="textTwitter" placeholder="cristian.cosenza" type="text">
				        <span class="help-block">Inserisci solamente il nome utente che appare nella barra degli indirizzi, escludendo http://twitter.com/ (es. cristian.cosenza)</span>
				      </div>
				    </div>
				    <div class="form-group">
				      <label for="inputEmail" class="col-lg-2 control-label">Canale YouTube:</label>
				      <div class="col-lg-10">
				        <input class="form-control" name="textYoutube" placeholder="Google" type="text">
				        <span class="help-block">Inserisci solamente il nome utente che appare nella barra degli indirizzi, escludendo http://youtube.com/ (es. Google)</span>
				      </div>
				    </div>
				    <div class="form-group">
				      <label for="inputEmail" class="col-lg-2 control-label">Profilo GitHub:</label>
				      <div class="col-lg-10">
				        <input class="form-control" name="textGitHub" placeholder="xL10n" type="text">
				        <span class="help-block">Inserisci solamente il nome utente che appare nella barra degli indirizzi, escludendo http://github.com/ (es. xL10n)</span>
				      </div>
				    </div>
                    <div class="form-group">
				      <label for="inputEmail" class="col-lg-2 control-label">Link avatar:</label>
				      <div class="col-lg-10">
				        <input class="form-control" name="textAvatar" placeholder="http://imgur.com/xxxxxx" type="text">
				      </div>
				    </div>
				    <div class="form-group">
				      <div class="col-lg-10 col-lg-offset-2">
				        <button type="reset" class="btn btn-default">Reset</button>
				        <button name="btnChangeSettings" type="submit" class="btn btn-primary">Cambia</button>
				      </div>
				    </div>
				</form>
		  </div>
		</div>

	</div>

    <div id="boxLogs" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Ultimi accessi</h4>
          </div>
          <div class="modal-body">
            <p><?php 
                foreach($CDatabase->connectionHandle->query("SELECT * FROM accesslogs WHERE AuthorId = ".$CDatabaseOperations->getUserId($CDatabase, $_SESSION["surname"])." ORDER BY Date DESC LIMIT 10") as $result) 
                     echo("Il ".((new DateTime($result["Date"]))->format("d-m-Y H:i:s"))." da ".$result["Ip"]."<br />");
            ?></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
          </div>
        </div>

      </div>
    </div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>