<?php

namespace Classe\Core\Extensions;
include_once(__DIR__."/../Activity.class.php");
use Classe\Core\Activity;

abstract class ActivityId extends Activity {

	const
		UPLOAD_FILE = 1,
        NEW_TOPIC = 2;

}

?>