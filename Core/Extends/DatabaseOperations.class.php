<?php

namespace Classe\Core\Extensions;
include_once(__DIR__."/../Database.class.php");
use Classe\Core\Database;
use PDO;

class DatabaseOperations extends Database {

	# Trova l'id dell'utente fornendo il cognome
	public function getUserId($connection, $surname) {
		$authorQuery = $connection->connectionHandle->prepare("SELECT Id FROM users WHERE Surname = ? LIMIT 1");
		$authorQuery->execute(array($surname));
		$authorResult = $authorQuery->fetch(PDO::FETCH_ASSOC);
		return $authorResult["Id"];
	}

	# Trova il cognome dell'utente fornendo l'id
	public function getUserSurname($connection, $id) {
		$authorQuery = $connection->connectionHandle->prepare("SELECT Surname FROM users WHERE Id = ? LIMIT 1");
		$authorQuery->execute(array($id));
		$authorResult = $authorQuery->fetch(PDO::FETCH_ASSOC);
		return $authorResult["Surname"];
	}

	# Controlla se l'utente è un professore
	public function isUserTeacher($connection, $username) {
		$authorQuery = $connection->connectionHandle->prepare("SELECT Teacher FROM users WHERE Surname = ? LIMIT 1");
		$authorQuery->execute(array($username));
		$authorResult = $authorQuery->fetch(PDO::FETCH_ASSOC);
		return (($authorResult["Teacher"]) ? (true) : (false));
	}

	# Trova l'avatar dell'utente
	public function getUserAvatar($connection, $username) {
		$authorQuery = $connection->connectionHandle->prepare("SELECT Avatar FROM users WHERE Surname = ? LIMIT 1");
		$authorQuery->execute(array($username));
		$authorResult = $authorQuery->fetch(PDO::FETCH_ASSOC);
		return $authorResult["Avatar"];
	}

	# Trova la proprietatà dell'utente indicata
	public function getUserProperty($connection, $username, $property) {
		$authorQuery = $connection->connectionHandle->prepare("SELECT * FROM users WHERE Surname = ? LIMIT 1");
		$authorQuery->execute(array($username));
		$authorResult = $authorQuery->fetch(PDO::FETCH_ASSOC);
		return $authorResult[$property];
	}

}

?>