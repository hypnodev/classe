<?php

namespace Classe\Core;
use PDO;

class Database {

    public $connectionHandle;

    public function __construct() {
        $this->connectionHandle = null;
    }

    # Funzione per connettersi al database
    public function openConnection($hostname, $username, $database, $password) {
        try {
            $this->connectionHandle = new PDO("mysql:host=$hostname;dbname=$database;charset=utf8", "$username", "$password");
            $this->connectionHandle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connectionHandle->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            return true;
        }
        catch(PDOException $e) {
            echo($e->getMessage());
            return false;
        }
    }

    # Chiude la connessione al database
    public function closeConnection() {
        if($this->connectionHandle != null && $this->isConnected) {
            $this->connectionHandle = null;
        }
        return $this;
    }    
}

?>