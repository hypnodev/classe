<?php

namespace Classe\Core;
require_once("Database.class.php");
require_once("Activity.class.php");
require_once("Extends/DatabaseOperations.class.php");
require_once("Extends/ActivityId.class.php");
use Classe\Core\Database;
use Classe\Core\Activity;
use Classe\Core\Extensions\DatabaseOperations;
use Classe\Core\Extensions\ActivityId;
use PDO;

class Topic {
    
    public function addTopic($connection, $title, $authorid, $content) {
        $topicQuery = $connection->connectionHandle->prepare("INSERT INTO topics(Title, AuthorId, Content) VALUES(?, ?, ?)");
        $topicQuery->execute(array($title, $authorid, $content));
        $idQuery = $connection->connectionHandle->prepare("SELECT Id FROM topics WHERE title = ?");
        $idQuery->execute(array($title));
        $idResult = $idQuery->fetch(PDO::FETCH_ASSOC);
        $activityQuery = (new Activity())->addActivity($connection, $authorid, Extensions\ActivityId::NEW_TOPIC, $idResult["Id"]);
    }

	public function getTopicInfo($connection, $topicid, $info) {
		$topicQuery = $connection->connectionHandle->prepare("SELECT * FROM topics WHERE Id = ? LIMIT 1");
		$topicQuery->execute(array($topicid));
		$topicResult = $topicQuery->fetch(PDO::FETCH_ASSOC);
		return $topicResult[$info];
	}

	public function getCommentInfo($connection, $commentid, $info) {
		$topicQuery = $connection->connectionHandle->prepare("SELECT * FROM comments WHERE Id = ? LIMIT 1");
		$topicQuery->execute(array($commentid));
		$topicResult = $topicQuery->fetch(PDO::FETCH_ASSOC);
		return $topicResult[$info];
	}

}

?>