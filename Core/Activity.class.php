<?php

namespace Classe\Core;
require_once("Database.class.php");
use Classe\Core\Database;
use PDO;
use DateTime;

class Activity {

	# Aggiunge una nuova attività svolta nel database
	public function addActivity($connection, $authorid, $activityid, $description) {
        $authorQuery = $connection->connectionHandle->prepare("INSERT INTO activity(AuthorId, ActivityId, Description, Datetime) VALUES (?, ?, ?, ?)");
        $authorQuery->execute(array($authorid, $activityid, $description, (new DateTime())->format("m-d-Y H:i")));
	}

	# Mostra le ultime attività dall'ultima alla più vecchia
	public function getActivityInfo($connection, $id, $infoid) {
		$activityQuery = $connection->connectionHandle->prepare("SELECT * FROM activity WHERE Id = ? LIMIT 1");
		$activityQuery->execute(array($id));
		$activityResult = $activityQuery->fetch(PDO::FETCH_ASSOC);
		return $activityResult[$infoid];
	}
}

?>