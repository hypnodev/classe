<?php

namespace Classe\Core;

class Utils {

    # Converte la grandezza del file in una misura comprensibile
    public function fsize($file) {
        $a = array("B", "KB", "MB", "GB", "TB", "PB");
        $pos = 0;
        $size = filesize("Uploads/".$file);
        while ($size >= 1024) {
                $size /= 1024;
                $pos++;
        }
        return round($size,2)." ".$a[$pos];
    }

    # Trova l'ip del visitatore (operatori ternari)
    public function getClientIP() {
        $ip = "0.0.0.0";
        $ip = getenv('HTTP_CLIENT_IP') ?: getenv('HTTP_X_FORWARDED_FOR')?: getenv('HTTP_X_FORWARDED')?: getenv('HTTP_FORWARDED_FOR')?: getenv('HTTP_FORWARDED')?: getenv('REMOTE_ADDR');
        return $ip;
    }
}

?>