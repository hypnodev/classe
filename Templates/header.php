<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="home.php">3° A Informatica</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav navbar-right">
        <?php
        foreach($CDatabase->connectionHandle->query("SELECT * FROM navbar ORDER BY Id ASC") as $result)
            echo "<li><a href=".$result["Url"].">".$result["Name"]."</a></li>";
        ?>
      </ul>
    </div>
  </div>
</nav>