<?php

namespace Classe;
include_once("Start.php");
use PDO;

session_start();

if(!isset($_SESSION["surname"])) {
	header("location: index.php");
	exit;
}


?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>3°A Informatica</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/default.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<div class="container">

		<?php include("Templates/header.php"); ?>

        <div style="float: left;"><h3>Lista discussioni</h3></div> <div style="float: right;"><a href="createtopic.php" class="btn btn-success">Crea discussione</a></div>

        <table class="table table-striped table-hover ">
          <thead>
            <tr>
              <th>#</th>
              <th>Autore</th>
              <th>Titolo</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
             <?php
                foreach($CDatabase->connectionHandle->query("SELECT * FROM topics ORDER BY Id DESC") as $result) {
                    $authorQuery = $CDatabase->connectionHandle->prepare("SELECT * FROM users WHERE Id = ? LIMIT 1");
                    $authorQuery->execute(array($result["AuthorId"]));
                    $authorResult = $authorQuery->fetch(PDO::FETCH_ASSOC);
                    echo("<tr ".(($authorResult["Teacher"]) ? ("class=\"warning\"") : ("")).">".
                            "<td>".$result["Id"]."</td>".
                            "<td>".(($authorResult["Teacher"]) ? ("Prof. ") : ("")).$authorResult["Surname"]."</td>".
                            "<td>".$result["Title"]."</td>".
                            "<td><a href=\"showtopic.php?id=".$result["Id"]."\" class=\"btn btn-primary btn-xs\">Apri</td>".
                            "</tr>"
                        );
                }
              ?>
          </tbody>
        </table> 

    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>