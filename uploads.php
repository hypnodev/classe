<?php

namespace Classe;
include_once("Start.php");
use PDO;
use DateTime;

session_start();

$resultMsg = "";

if(!isset($_SESSION["surname"])) {
	header("location: index.php");
	exit;
}

if(isset($_POST["btnUpload"])) {
	if(isset($_FILES["fileName"])) {
		$file = $_FILES["fileName"]; 
		if($file["error"] === UPLOAD_ERR_OK && is_uploaded_file($file["tmp_name"])) {
			if(file_exists($file["name"])) $resultMsg = "Il file esiste già";
			else {
				if($file["size"] > 0) {
					move_uploaded_file($file["tmp_name"], "Uploads/".$file["name"]);
					$result = $CDatabase->connectionHandle->prepare("INSERT INTO uploads (AuthorId, File) VALUES(?, ?)");
	    			$result->execute(array($CDatabaseOperations->getUserId($CDatabase, $_SESSION["surname"]), $file["name"]));
	    			$activity = $CDatabase->connectionHandle->prepare("INSERT INTO activity (AuthorId, ActivityId, Description, Datetime) VALUES(?, ?, ?, ?)");
	    			$activity->execute(array($CDatabaseOperations->getUserId($CDatabase, $_SESSION["surname"]), Core\Extensions\ActivityId::UPLOAD_FILE, $file["name"], (new DateTime())->format("m-d-Y H:i")));
	    			$resultMsg =  "File caricato con successo!";
	    		} else $resultMsg = "Non puoi caricare file vuoti.";
			}
		}
	}
}

?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>3°A Informatica</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/default.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<div class="container">

	    <?php include("Templates/header.php");
	    if($resultMsg != "") {
	    if($resultMsg === "Non puoi caricare file vuoti." || $resultMsg === "Il file esiste già ") { ?>
	    	<div class="alert alert-dismissible alert-danger">
	    <?php } else { ?>
	    	<div class="alert alert-dismissible alert-success">
	    <?php } ?>	    
	    		<button type="button" class="close" data-dismiss="alert">x</button>
	    		<?php echo($resultMsg); ?>
			</div>
		<?php } ?>

		<div class="row">
			<div class="col-xs-6 col-md-4">
				<div class="panel panel-default">
				  <div class="panel-body">
				   	<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">   
						<input type="file" name="fileName" class="btn btn-default" /> <br />   
						<input type="submit" value="Carica file" name="btnUpload" class="btn btn-success"/>   
					</form> 
				  </div>
				</div> 
			</div>

			<div class="col-xs-12 col-md-8">
				<table class="table table-striped table-hover ">
				  <thead>
				    <tr>
				      <th>#</th>
				      <th>File</th>
				      <th>Caricato da</th>
				      <th>&nbsp;</th>
				    </tr>
				  </thead>
				  <tbody>
				  <?php
					foreach($CDatabase->connectionHandle->query("SELECT * FROM uploads ORDER BY Id DESC") as $result) {
						$authorQuery = $CDatabase->connectionHandle->prepare("SELECT * FROM users WHERE Id = ? LIMIT 1");
    					$authorQuery->execute(array($result["AuthorId"]));
    					$authorResult = $authorQuery->fetch(PDO::FETCH_ASSOC);
						echo("<tr ".(($authorResult["Teacher"]) ? ("class=\"warning\"") : ("")).">".
								"<td>".$result["Id"]."</td>".
								"<td>".$result["File"]."</td>".
								"<td>".(($authorResult["Teacher"]) ? ("Prof. ") : ("")).$authorResult["Surname"]."</td>".
								"<td><a href=\"download.php?file=Uploads/".$result["File"]."\" class=\"btn btn-primary btn-xs\">Scarica</td>".
								"</tr>"
							);
					}
				  ?>
				  </tbody>
				</table> 
			</div>
		</div>

	</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>