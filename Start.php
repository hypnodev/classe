<?php

namespace Classe;
require_once("Core/Database.class.php");
require_once("Core/Activity.class.php");
require_once("Core/Utils.class.php");
require_once("Core/Topic.class.php");
require_once("Core/Extends/DatabaseOperations.class.php");
require_once("Core/Extends/ActivityId.class.php");
require_once("Core/API/GitHub/GitHubClient.php");
use PDO;
use Classe\Core\Database;
use Classe\Core\Activity;
use Classe\Core\Extensions\DatabaseOperations;
use Classe\Core\Extensions\ActivityId;
use Classe\Core\Utils;
use Classe\Core\Topic;

define("DB_HOSTNAME", "127.0.0.1");
define("DB_USERNAME", "root");
define("DB_DATABASE", "classe");
define("DB_PASSWORD", "");

$CDatabase = new Database();
$CDatabaseOperations = new DatabaseOperations();
$CActivity = new Activity();
$CUtils = new Utils();
$CGitHub = new \GitHubClient();
$CTopic = new Topic();

$CDatabase->openConnection(DB_HOSTNAME, DB_USERNAME, DB_DATABASE, DB_PASSWORD);

?>