# README #

> Progetto del sito della classe 3A Informatica.

> Tutto questo è stato creato usando PHP5, PDO/MySQL, HTML5 e CSS3.

> Per il tema si ringrazia Bootswatch (https://bootswatch.com/).

> Distribuito sotto licenza GNU GPL v3 (http://www.gnu.org/licenses/gpl-3.0.txt)

### Cartelle ###

* Core: E' il "cuore" del progetto, non modificare i file di questa cartella.
* Core/API: Contiene tutte le API per comunicare con applicazione esterne.
* css, font, images, js: Contengono tutti i file che riguardano i layout.
* Templates: Qui ci sono le parti di file che vengono contenuti in tutti i file che vengono mostrati.
* Uploads: Vengono inseriti tutti i file caricati.