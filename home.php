<?php

namespace Classe;
include_once("Start.php");

session_start();

if(!isset($_SESSION["surname"])) {
	header("location: index.php");
	exit;
}

?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>3°A Informatica</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/default.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<div class="container">

	    <?php include("Templates/header.php"); ?>

		<div class="row">
			<div class="col-xs-12 col-md-8">
				<?php foreach($CDatabase->connectionHandle->query("SELECT * FROM activity ORDER BY Id DESC LIMIT 10") as $result) {
			    	echo("<div class=\"panel panel-default\">
							  <div class=\"panel-heading\">
							    <h3 class=\"panel-title\"><a href=\"profile.php?id=".$CActivity->getActivityInfo($CDatabase, $result["Id"], "AuthorId")."\"><img src=\"".$CDatabaseOperations->getUserAvatar($CDatabase, $_SESSION["surname"])."\" class=\"avatar\" /> ".$CDatabaseOperations->getUserSurname($CDatabase, $CActivity->getActivityInfo($CDatabase, $result["Id"], "AuthorId"))."</a> (".$CActivity->getActivityInfo($CDatabase, $result["Id"], "Datetime").")</h3>
							  </div>
							  <div class=\"panel-body\">
						");
							    switch($CActivity->getActivityInfo($CDatabase, $result["Id"], "ActivityId")) {
							    	case Core\Extensions\ActivityId::UPLOAD_FILE:
							    		echo($CDatabaseOperations->getUserSurname($CDatabase, $CActivity->getActivityInfo($CDatabase, $result["Id"], "AuthorId"))." ha caricato il file <a href=\"download.php?file=Uploads/".$CActivity->getActivityInfo($CDatabase, $result["Id"], "Description")."\">".$CActivity->getActivityInfo($CDatabase, $result["Id"], "Description")."</a>");
							    		break;
                                    case Core\Extensions\ActivityId::NEW_TOPIC:
                                        echo($CDatabaseOperations->getUserSurname($CDatabase, $CActivity->getActivityInfo($CDatabase, $result["Id"], "AuthorId"))." ha creato il topic <a href=\"showtopic.php?id=".$CActivity->getActivityInfo($CDatabase, $result["Id"], "Description")."\">".$CTopic->getTopicInfo($CDatabase, $CActivity->getActivityInfo($CDatabase, $result["Id"], "Description"), "Title")."</a>");
                                        break;
							    }
					echo("</div>
							</div>
			    		");
			    } ?>
			</div>

			<div class="col-xs-6 col-md-4">
				<div class="list-group">
					<?php foreach($CDatabase->connectionHandle->query("SELECT * FROM uploads ORDER BY Id DESC LIMIT 5") as $result) {
					  echo("<a href=\"download.php?file=Uploads/".$result["File"]."\" class=\"list-group-item\">
					    <h4 class=\"list-group-item-heading\">".$result["File"]."</h4>
					    <p class=\"list-group-item-text\"><b>Caricato da: </b>".$CDatabaseOperations->getUserSurname($CDatabase, $result["AuthorId"])."&nbsp;<b>Peso: </b>".$CUtils->fsize($result["File"])."</p>
					  </a>");
				  	} ?>
				</div>
			</div>
		</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>